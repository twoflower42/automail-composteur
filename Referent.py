class Referent:
    """Définis les attributs d'un referent"""
    
    def __init__(self, nom, mail, telephone):
        self.nom = nom
        self.mail = mail
        self.telephone = telephone
