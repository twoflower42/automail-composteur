import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class Mail:
    """Classe de gestion de l'envoi du mail de rappel aux permanents"""

    def __init__(self, mailEmetteur='', mailReply='', serveurSMTP='',
                 loginSMTP='', passSMTP='',
                 portSMTP=587, methodeSMTP='TLS', mailDest='',
                 sujetMessage='', texteMessage=''):
        """A l'initialisation on ne déclare que le strict minimum :\
        emetteur, serveur SMTP et login et mot de passe correspondants"""

        self.mailEmetteur = mailEmetteur
        self.mailReply = mailReply
        self.serveurSMTP = serveurSMTP
        self.loginSMTP = loginSMTP
        self.passSMTP = passSMTP
        self.portSMTP = portSMTP
        self.methodeSMTP = methodeSMTP
        self.mailDest = mailDest
        self.sujetMessage = sujetMessage
        self.message = texteMessage

    def __envoieMailTLS(self, messageMIME):
        print("ping")
        mailserver = smtplib.SMTP(self.serveurSMTP, self.portSMTP)
        mailserver.starttls()
        mailserver.login(self.loginSMTP, self.passSMTP)
        mailserver.sendmail(self.mailEmetteur, self.mailDest,
                            messageMIME.as_string())
        mailserver.quit()

    def __envoieMailSSL(self, messageMIME):
        mailserver = smtplib.SMTP_SSL(self.serveurSMTP, self.portSMTP)
        mailserver.login(self.loginSMTP, self.passSMTP)
        mailserver.sendmail(self.mailEmetteur, self.mailDest,
                            messageMIME.as_string())
        mailserver.quit()

    def __envoieMailSimple(self, messageMIME):
        mailserver = smtplib.SMTP(self.serveurSMTP, self.portSMTP)
        mailserver.login(self.loginSMTP, self.passSMTP)
        mailserver.sendmail(self.mailEmetteur, self.mailDest,
                            messageMIME.as_string())
        mailserver.quit()

    def envoi_mail(self):
        """Envoie le mail"""

        msg = MIMEMultipart()

        msg['Subject'] = self.sujetMessage

        if(self.mailEmetteur != ''):
            msg['From'] = self.mailEmetteur
        else:
            raise ValueError("Pas d'émetteur spéficié")

        if(self.mailReply != ''):
            msg['Reply-To'] = self.mailReply
        else:
            msg['Reply-To'] = self.mailEmetteur

        if(self.mailDest != ''):
            msg['To'] = self.mailDest
        else:
            raise ValueError("Pas de destinataire spéficié")

        msg.attach(MIMEText(self.message))

        if((self.portSMTP == 587 and self.methodeSMTP == '') or
           self.methodeSMTP == 'TLS'):
            self.__envoieMailTLS(msg)

        elif((self.portSMTP == 465 and self.methodeSMTP == '') or
             self.methodeSMTP == 'SSL'):
            self.__envoieMailSSL(msg)

        elif((self.portSMTP == 25 and self.methodeSMTP == '') or
             self.methodeSMTP == 'simple'):
            self.__envoieMailSimple(msg)
