import datetime
import Referent
import re
from datetime import date

class Permanence:
    """ Classe définissant une permanence"""

    regex_date = '(lundi|mardi|mercredi|jeudi|vendredi|samedi|'\
                'dimanche)\s([1-2][0-9]|3[0-1]|[1-9])\s'\
                '(janvier|février|mars|avril|mai|juin|août|septembre'\
                '|octobre|novembre|décembre)\s[0-9]{4}'
    mois_numero = {'janvier': 1, 'février': 2, 'mars': 3, 'avril': 4,
                   'mai': 5, 'juin': 6, 'juillet': 7, 'août': 8,
                   'septembre': 9, 'octobre': 10, 'novembre': 11,
                   'décembre': 12}
    
    def __init__(self, referent1, referent2, date_perm_str):
        self.referent1 = referent1
        self.referent2 = referent2
        self.__date_perm = None
        self.date_perm = date_perm_str
        
    @property
    def date_perm(self):
        """Getter pour la date"""
        return self.__date_perm

    @date_perm.setter
    def date_perm(self, date_perm_str):
        """
        Setter pour la date, vérifie que la string passée en entrée est format
        "JourSemaine JourMois Mois Annee"
        """
        if(Permanence.est_date(date_perm_str)):
            self.__date_perm = Permanence.parse_date(date_perm_str)
        else:
            self.__date_perm = None

    def est_date(date_perm_str):
        """
        Vérifie qu'une date est au format "JourSemaine JourMois Mois Annee". 
        Par exemple "Lundi 3 janvier 2017"
        """
        return re.match(Permanence.regex_date, date_perm_str)

    def parse_date(date_perm_str):
        """
        Parse une date au format "JourSemaine JourMois Mois Annee". 
        Par exemple "Lundi 3 janvier 2017"
        """
        liste_date_perm = date_perm_str.split(' ')
        return date(int(liste_date_perm[3]),
                    Permanence.mois_numero[liste_date_perm[2]],
                    int(liste_date_perm[1]))
