import os
import csv
from Mail import *
from ListePerm import *
from configparser import ConfigParser


class automail:
    def __init__(self, config_path):
        config_automail = ConfigParser()
        try:
            config_automail.read(config_path)
        except:
            print("Fichier config.ini introuvable ou mal formaté")
        self.config = config_automail

    def envoi_rappel_referent(self, date_perm, referent, sujet_message, corps_message):
        if(referent != None):
            if(referent.mail != None):
                mail_referent = Mail(
                    self.config['MAIL']['adresse'],
                    self.config['MAIL']['reply'],
                    self.config['SMTP']['adresse'],
                    self.config['SMTP']['login'],
                    self.config['SMTP']['pass'],
                    self.config['SMTP']['port'],
                    self.config['SMTP']['methode'],
                    referent.mail,
                    sujet_message,
                    corps_message)

                mail_referent.envoi_mail()
            else:
                print(str(date) + " : ATTENTION Pas de mail pour le " +
                      "référent " + prochaine_perm.referent1.nom)
        else:
            print(str(date_perm) + " : ATTENTION Un référent est manquant")
         
    def envoi_rappels(self):

        # selection de la prochaine permanence
        try:
            with open(self.config['CSV']['csv_location']) as csv_file:
                liste_permanences = ListePerm(csv_file)
        except:
            raise IOError("Le fichier CSV indiqué est introuvable")
            
        prochaine_perm = liste_permanences.trouve_prochaine_perm()

        # chargement des paramètres du message et du sujet
        try:
            with open(self.config['MAIL']['fichier_sujet']) as fic_sujet:
                sujet_message = fic_sujet.read()
        except:
            raise IOError("Le fichier du sujet est introuvable")
        
        try:
            with open(self.config['MAIL']['fichier_texte']) as fic_corps:
                corps_message = fic_corps.read()
        except:
            raise IOError("Le fichier du corps est introuvable")

        self.envoi_rappel_referent(prochaine_perm.date_perm, prochaine_perm.referent1,
                                   sujet_message, corps_message)
        self.envoi_rappel_referent(prochaine_perm.date_perm, prochaine_perm.referent2,
                                   sujet_message, corps_message)

   

def main():
    # change path to script directory
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)

    auto_mails = automail("config.ini")

    auto_mails.envoi_rappels()
  
if __name__ == "__main__":
    main()
