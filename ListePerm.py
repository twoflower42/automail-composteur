import re

from datetime import date
import csv
from Permanence import *
from Referent import *

class ListePerm:
    """Classe de gestion de la liste des bénévoles"""

    def __init__(self, csv_file):
        csv_reader = csv.reader(csv_file)
        self.liste_perms = []
        self.importe_csv(csv_reader)

    def get_column_row(row, i):
        """
        Retourne l'élément d'une liste en vérifiant que cet élément existe bien
        """
        if(i < len(row)):
            return row[i]
        else:
            return None


    def importe_csv(self, csv_reader):
        """
        Lit chaque ligne d'un reader CSV et les ajoute à la liste
        """
        i = 0
        for row in csv_reader:
            ref1 = Referent(ListePerm.get_column_row(row,2),
                                     ListePerm.get_column_row(row,3),
                                     ListePerm.get_column_row(row,4))
            ref2 = Referent(ListePerm.get_column_row(row,5),
                                     ListePerm.get_column_row(row,6),
                                     ListePerm.get_column_row(row,7))
            perm = Permanence(ref1, ref2, ListePerm.get_column_row(row,0)) 
            if(perm.date_perm != None):
                self.liste_perms.append(perm)

    def trouve_prochaine_perm(self):
        """
        Trouve la prochaine permanence à partir de la date du jour
        """
        aujourdhui = date.today()
        i = 0
        for perm in self.liste_perms:
            if(perm.date_perm > aujourdhui):
                break
            i += 1
        return self.liste_perms[i]
